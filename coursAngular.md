## page app.component.ts
```
export class AppComponent implements OnInit {

  title = 'myAngular';

  avatarUrlFromMain = 'https://www.w3schools.com/howto/img_avatar.png';

  vote = 0;

  pageInfo = {
    title: 'myAngular',
    date: '12/05/2001',
  }

  lastName = 'bobo2';
  hostels: HotelModel[];

  constructor(
    private hostelsService: HostelsService,
  ) {
  }
changeTitle(pageInfo:{title: string; date: string}){
    pageInfo.title = 'toto';
}

  getHostels() {
    this.hostelsService.getHostels$()
      .pipe(
        tap((hostels: HotelModel[])=> this.hostels = hostels),
        tap((hostels: HotelModel[])=> console.log(this.hostels)),
      )
      .subscribe()
  }

  ngOnInit(): void {
    this.getHostels();
    this.changeTitle(this.pageInfo);
  }

  changeVoteValue(newVoteValue: number) {
    this.vote = newVoteValue
  }
}
```
## page app.component.html
```
 <h1>{{title}}</h1>
 <h1>{{pageInfo.title}}</h1>
 
 <app-avatar
   [avatarUrl]="avatarUrlFromMain"
   [rating]="10"
   (isVotedDone)="changeVoteValue($event)"
 >
 
 
 </app-avatar>
 
 <p>{{vote}}</p>
 
 <input type="text" [(ngModel)]="lastName">
 
 <ul>
   <li *ngFor ="let hostel of hostels; let i = index">
     {{i}} {{hostel.name}} {{hostel.roomNumbers}} {{hostel.pool}}
   </li>
 </ul>
```
