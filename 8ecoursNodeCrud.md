 # NODE/CRUD
Dans un 1er temps, nous allons utiliser le node avec serveur.  

![definitionCrud](/uploads/92898b4da3c1027d55dbd42516261854/definitionCrud.PNG)

Pour connaître la version de Node utilisée: 
 
![versionNode](/uploads/a04da43c1bf8645da40b915643bca8ba/versionNode.PNG)

Ensuite, on va installer le **npm**(gestionnaire de paquet) qui est en quelque sorte un playstore pour node.  

**Remarque**: vérifiez d'abord que le **npm** n'est pas encore installé en regardant si le fichier package.json est déjà présent.
  
![packageJson](/uploads/2a59e2c6ce0a3b774633aa033bccdaa2/packageJson.PNG)

Dans le package json, on va pouvoir différencier 2 types d'informations, les **dependencies et les devDependencies**.  

![dependencies](/uploads/501cf68b327275abd9588f6fce246c7e/dependencies.PNG)

Les **devDependencies** sont les applications dont on va avoir besoin pour développer.  
Les **dependencies** sont les applications qui vont être utilisées dans votre application finale par le client.
Un exemple, le css-loader que l'on aperçoit coté devDependencies sont les fichiers qui permettent de charger les feuilles de styles.  
L'utilisateur n'a donc pas besoin de les voir.

Une fois le package.json installé(si on ne l'a pas déjà),on peut installer les autres modules.  
Dans notre cas **express**, `**npm i -S express**`car on veut que notre framework aille dans les **dependencies**:  
![express](/uploads/d6b6c5801e8ac3cba3f7075d4bf8b6a5/express.PNG)  

Pour mettre le framework dans le `**devDependencies: npm i -D express**`.  
**Remarque**: vous pouvez chercher sur le net n'importe quel framework en tapant **npm i**.  

Pour notre confort de travail, on utilisera l'autocompression pour node.  
Pour ce faire, on sélectionne dans le webstorm, **File puis Settings** et:  

![codingAssist](/uploads/50303f65e8f2be5243650d3e6e132e6d/codingAssist.PNG)

On peut alors utiliser node, **new file et le nommer index.js**.
Par conséquent, pour l'exécuter:  

![nodeIndex](/uploads/244b69d57f0985faaf7992d0ee039900/nodeIndex.PNG)  

Pour profiter des fonctionnalités **express**, on déclare le**const express et le require**,   
par convention en haut de page.  

![applisten](/uploads/575cdb3c912cfc5f2458fa320970b8a8/applisten.PNG)

Ici, on crée une application qui va écouter la racine**'/' et le port 3000**(endroit disponible pour que le pc soit accesible par internet).  
Si on se positionne dans le terminal, on voit que ça écoute dans le port 3000.  
On va alors dans le navigateur et on saisit **localhost:3000**(**local** signifie pour mon pc et **host**, héberger par) puis on valide.  
On obtiendra alors le résultat de la fonction, ici "hello world".  

**Attention**On doit systématiquement relancer le serveur avec **ctrl c**(pour le début de notre apprentissage).  
**Important**Dans un **GET**, il ne peut y avoir que des strings(attention au number).  
Ce qui nous intéresse c'est de renvoyer des `json`.  

 ## QU'EST CE QUE LE JSON?

![json](/uploads/08b3090307b2db286c0306172f33b7fa/json.PNG)  

L'affichage dans chrome n'est pas fait pour du Json.
Afin de pouvoir utiliser la fonction **GET** depuis le navigateur et pour notre exemple, sélectionner les éléments d'un hotel:  

![appGet](/uploads/8a2938e793f485a5d61a5edcac133234/appGet.PNG)
Après avoir copié notre fonction, on l'adapte au `GET`.
Ici, la route s'appelle hostels, `res` est l'outil que l'on utilise pour répondre à l'utilisateur, on s'en sert en faisant `res.send`.  
`req` permet de récupérer la requête faite par l'utilisateur. Le ':id'pour la variable qui va être stockée sous le nom `id`.  
`req.params.id` signifie que les infos demandées par l'utilisateur vont être stockées dans `id`.  
`parseInt` pour changer le number en string.

 ## QU'EST QUE LE POSTMAN?
Pour utiliser **PUT, DELETE, CREATE**.

![postman](/uploads/941f19d389caa4cb0484fca72798aac8/postman.PNG)  

Par exemple, l'utilisation d'un `GET`.  

![appGet](/uploads/9680a5f37b1c5c5d0172935ffc540ee7/appGet.PNG)

On copie la fonction du delete(removeHostels) puis on l'adapte.  

![delete](/uploads/52d52b70222252140312bb058e71efbc/delete.PNG)

`app.put` pour modidier un objet et `app.post` pour créer un objet.





