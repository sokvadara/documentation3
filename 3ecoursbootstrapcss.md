# COMMENT PERSONNALISER BOOTSTRAP?
Ce qu'on souhaite faire c'est changer le dossier bootstrap à notre goût.
Par exemple je souhaite changer la couleur du button en orange, on dit alors que l'on va **"surcharger la feuille de style"** bootstrap pour la mettre à notre goût.

![buttonjaune](/uploads/3198a1493b10834310678b08018d458e/buttonjaune.PNG)

La première chose c'est d'aller dans **assets** et créer un nouveau dossier que l'on nomme "styles", dans lequel on créer un fichier appelé "bootstrap.css".
Ce fichier doit être ajouté après le lien bootstrap afin de pouvoir modifier le contenu en cas de conflit d'un même emploi.
Ici on copie le lien bootstrap puis on l'adapte pour le fichier bootstrap.css  

![linkbootcss](/uploads/11f71c807318e9326772e377c92ae2b3/linkbootcss.PNG)

Le fichier étant pris en compte, on peut aller dans la **console et copier/coller la "class du button"** pour pouvoir la modifier:

![copiécollébutton](/uploads/b0e89c7617ac49252de84769982df9ed/copiécollébutton.PNG)

![copiécolléboot](/uploads/a23dfca7ab5fae655d73fa0c3dff26af/copiécolléboot.PNG)

puis on modifie la couleur en double-cliquant sur le carré couleur.

![modifcouleur](/uploads/89f65b5c9bfd617d0298437c279fa402/modifcouleur.PNG)

## COMMENT BIEN ORGANISER SES FEUILLES DE STYLES? LES IMPORTS.
Pour éviter d'avoir à rajouter un liste interminable de liens, on utilise les imports c'est à dire que l'on va garder seulement un seul fichier css pour y mettre tous les imports.

![import](/uploads/c558c62605c77c9ba90595c670caf38e/import.PNG)

## COMMENT METTRE SES PROPRES FEUILLES DE STYLES?

On peut rassembler les feuilles de styles sous un même fichier.
D'abord on s'assure que les diférents fichiers bootstrap sont déjà "commit and push".
La procédure est de copier les feuilles, de les coller dans la nouvelle feuille et de les "commit and push".

![ajoutetmodif](/uploads/cba0668ad76a9e40c04ae006808af088/ajoutetmodif.PNG)

Ensuite supprimer les anciennes feuilles puis de "commit and push en signalant le changement lors du 2e push".

![movefiles](/uploads/cabc40d68d73004047191a21d49d35ad/movefiles.PNG)

Attention l'emplacement de la feuille a changé, il faut le modifier:

![emplacement](/uploads/2b985d87abf9c60a2e56b6e4439c7f50/emplacement.PNG)

**Remarque**: pas besoin d'importer les liens bootstrap/css dans tous les dossiers, on importe uniquement les class dont on a besoin.