# POST AND PUT
L'idée est de créer une route et de l'appeler depuis **postman** et d'avoir des données en retour.  
Pour ce faire, il faut importer les fonctions déjà créer et se servir du **rest** et du **crud**.

**Rappel:**  
Le **rest** est la façon d'organiser les url.
Le **crud** c'est le fait de créer, lire, mettre à jour et supprimer les données.  

## Utiliser POST  
Une méthode simple est de copier la fonction de call-back et de l'adapter.  
Etant donné que l'on veut faire le post dans le body, il nous manque quelques informations que l'on peut trouver dans la doc express concernant les app.  

![reqBody](/uploads/3a2b592b5b716d255076c5ad92421534/reqBody.PNG) 
que l'on colle dans notre index toujours tout en haut.

![codeExpress](/uploads/821169b2d07b1eac6cd34afce04d4a12/codeExpress.PNG)

Puis on copie-colle le nouvel hotel crée dans le body du postman, en sélectionnant **raw et json**.
**info pratique**, en **json** les string sont en double cotes(pas en simple cote), pas de double cotes pour les number et les booléans car `Json` les reconnaît et **;**.  
Exemple de début:  

![foncPost](/uploads/44fcae3f901c60abeaed9825cc3e8f53/foncPost.PNG)

L'`app.post` s'écrira alors ainsi(en changeant au préalable ce qu'il faut dans la fonction):  

![raw](/uploads/94db214939088bae9fcd624a420a3727/raw.PNG)

## Utiliser PUT

![appPut](/uploads/e29a3df1bc672618f8a0e6e9c3ec4cf5/appPut.PNG)
Ne pas oublier de rajouter le paramètre `id` parce que c'est ce qui va nous permettre de modifier.
`PUT` remplace l'intégralité d'un objet. `PATCH` va remplacer quelques attibuts d'un objet.
 
## LES CODES HTTP(liste des codes http sur wikipédia)
Les plus fréquentes sont les codes 200, 400 et 500.  

![code200](/uploads/3f838b537a39a61ba6a103429e835da2/code200.PNG)

![code400](/uploads/e84a218faa9b1e08d64b43fba7b3ef71/code400.PNG)  

![code500](/uploads/991566ca7d0cd38032765a52f34d5f13/code500.PNG)

