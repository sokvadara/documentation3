# Les FALSY

Les falsy sont des valeurs qui sont équivalentes à false.  
Il est important de savoir les repérer.  
On sait qu'on a un falsy quand il n'y a pas de réponse dans la console.
La liste des falsy:  

![listedesfalsy](/uploads/34c90f41f009f2e983fb92a0a3ba1431/listedesfalsy.PNG)

**Attention un string avec un espace n'est pas un string vide.**

Remarque, si on utilise le **!** qui signifie inverse, on peut aussi avoir aucun résultat dans la console:

![inverse](/uploads/70666fc7a9558526d2612358f12aca78/inverse.PNG)

Toutes les valeurs qui ne sont pas des **falsy** sont des **truthy**.

## TRANSFORMER UN OBJET EN TABLEAU

L'opérateur **delete**

Il permet de supprimer l'élement d'un objet notamment quand on l'a en double.
Ici, "my.tata" n'apparaitra plus dans la console.

![opérateurDelete](/uploads/11d93cf70bc35e517ef26782db704819/opérateurDelete.PNG)

Pour afficher les clés de l'objet dans la console:  

![afficheClé](/uploads/a6599f968409657534b2db9463532fcb/afficheClé.PNG)

![captata](/uploads/d7d771a3990d0509dd413a29b7c0dad2/captata.PNG)

Pour faciliter cette manipulation:

![foncToUppercase](/uploads/5f6a25e1455ba489f1aed16a99efba8b/foncToUppercase.PNG)

Dans la console, on a récupéré les clés:
  
![capMaj](/uploads/7579505546847982d2799272371e9bd6/capMaj.PNG)

et ici on récupère les valeurs:  

![valeur](/uploads/1005b386cbf0c177633c9f5436c7679c/valeur.PNG)

**Remarque**:  
On ne peut pas transformer tout un objet en tableau, on va changer ses clés ou ses valeurs.  
Ici, 2 tableaux avec d'un coté les valeurs et de l'autre les clés.  

![valeurEtClé](/uploads/2f2b3b0ba6b324d21db40c74927a5a9f/valeurEtClé.PNG)



