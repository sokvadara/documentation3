#     ![mdGuide](/uploads/da85c8bc432f63743d13f9638bed4096/mdGuide.PNG)
## Qu'est ce que le markdown?  
Le markdown est un langage de prise de notes.     
Attention HTML et CSS ne sont pas des langages de programmation mais des langages de markdown.    
**A SAVOIR**: le markdown existe partout, GITUB, GITLAB, SLACK... mais il s'écrit de différente manière selon le site.    

## Quelques outils
**Boiler plate** représente les informations qui sont déjà mis en place.  
**Bootstrap** est le nom du framework **CSS** utilisé par tweeter mais aussi le nom donné à un nouveau projet from scratch(de zéro), on dit alors que l'on va bootstrapper quelque chose.  
**WISIWIG**: **What I See Is What I Get**, c'est un éditeur qui permet directement d'éditer dans le web du contenu dans votre navigateur et qui sera sauvegardé dans la même forme que l'on a obtenu.  

## Comment structurer ma page?
Pour commencer, on fait empty project.  
Un des avantages de webstorm est d'avoir des pluggins notamment le markdown.    
Le markdown est en effet un langage de prise de notes ou de mise en forme de textes( comme un word simplifié).  
La logique en markdown est la même qu'en HTML:  
![headingsMd](/uploads/704e5c32061fdeafeb5d7392f39237e1/headingsMd.PNG)

Le bon réflexe à adopter étant de commencer par structurer son texte en plaçant le h1, h2 et h3 tout comme en HTML.  
Ne pas découper son texte en pensant à l'esthétique, ceci venant dans un second temps.  
Ce travail doit être fait dans un sens sémantique( comment mettre en écrit ce qu'on pense).  
Cette manière de faire s'apparente au **SEO**:  

![defSeo](/uploads/6831efae9657635c1f2e5e97c6747ab1/defSeo.PNG)

## Les backticks
**Un backtick** va juste mettre le texte en évidence.
**Les 3 backticks** pour intégrer du code dans une zone de texte:
```
const('coucou');
console.log('coucou');
```
## GIT, GITLAB, GITHUB
**GIT** est un protocole qui permet de stocker des informations/du contenu.
**GITHUB ET GITLAB** sont des sites internet qui utilisent le protocole GIT.

## Comment stocker mon markdown avec GITLAB?
Pour créer un nouveau projet, on va sur notre **GIT**:
![newProjecGit](/uploads/8ca05e19f234561a5d46964f3cf9e47c/newProjecGit.PNG)

Puis:

![nomDocGitPublic](/uploads/9be9d5f56981579b005fde637d5d0454/nomDocGitPublic.PNG)

Ensuite le lien de **GIT** pour notre **webstorm**:

![cloneHttps](/uploads/07c39bf462f4dad0d739e990f311bbd8/cloneHttps.PNG)

que l'on copie-colle en sélectionnant au préalable le **check out**:  

![checkoutVersionCtrl](/uploads/969817fb80738a998ab21dbb1b0c0a96/checkoutVersionCtrl.PNG)
![colleHttps](/uploads/f98506136246949c6a7b3b5bce956914/colleHttps.PNG)

Ensuite "test"(le nom et code GITLAB sera demandé) puis on valide sur "clône".
Une nouvelle fenêtre vous invite à créer un dossier:

![yesOuvrirProject](/uploads/ca7d9245a664839e8aba27a031cf24b8/yesOuvrirProject.PNG)

On va créer autant de dossiers que l'on va avoir de sujets(clique droit, new directory).

## Comment sauvegardez mes données sur GITLAB?
Deux étapes, la 1ere que l'on appelle **commit** est de sauvegarder sur la machine.  
La 2ème appelé **push** est d'envoyer sur **GITLAB**.  
Par conséquent on va **commiter** les changements que l'on a fait puis les **pusher**.  
Lors d'un commit, on explique toujours ce que l'on veut sauvegarder.   
Par exemple, structure de la page et création de l'architecture.   
On n'oublie pas de décocher les deux cases de droite puis **commit**.  
Ensuite pour **pusher**: **VCS, GIT, push** et c'est envoyé.   
Sur **GITLAB**, `ctrl R` et le document apparaît.  

