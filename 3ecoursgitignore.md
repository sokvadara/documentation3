# Cours de GITIGNORE
Il existe un certain nombre de fichiers qui sont cachés et qui contiennent des informations sur notre GIT et nos commits. 
Par exemple, le dossier ".idea" contient des fichiers de configuration concernant notre navigateur, notre webstorm.

Le "point" devant signifie que c'est un fichier caché( exemple: .vscode ou .éclipse), il n'est donc pas visible.
Pour les voir, on clique sur "project file"

![voirfichierparfile](/uploads/62e2d6af83bf2908727f4be6c1c4995b/voirfichierparfile.PNG)

![capture.idea](/uploads/863d63d13ea6b4a17168d1b5178dca40/capture.idea.PNG)
On peut voir ici le ".idéa" en vert, cela signifie qu'il ne sera pas pris en compte par GIT.
Cela veut dire que lors d'un push ou pull, le ".idéa" ne sera pas envoyé.

## Comment paramétrer le navigateur pour ne plus avoir de problèmes avec ce genre de fichiers?
On va créer un fichier qui s'appelle **.GITIGNORE**,on y placera tout ce qu'on ne veut pas envoyer.

Dans un premier temps, si on veut effacer, on clique droit puis "delete".

![delete_gitignore](/uploads/910e131698f600b8d133d2020cf58129/delete_gitignore.PNG)

Ensuite, pour créer "clique droit puis new file"

![Capefface.gitignore](/uploads/c5071788d9a56f00974bcbd0d68ae993/Capefface.gitignore.PNG)

puis on met un ".gitignore"

![créationgitignore](/uploads/34f587a826efa9f75661b8803dae5686/créationgitignore.PNG)

Et on l'ajoute à GIT("add").

Dans le dossier ".gitignore", on va saisir le dossier/fichier que l'on ne veut pas envoyer, ici".idéa":

![idéapasprGIT](/uploads/6935392d14af428cbc83f93159559a2b/idéapasprGIT.PNG)

Pour bien comprendre, vous sélectionnez "project" et tout ce qui n'est pas à envoyer dans GIT, qui se trouvent sur "project file, vous le saisissez dans "gitignore".

![project](/uploads/4ed789de1a81da6355452ee60b02d253/project.PNG)

Dans le cas d'un fichier que l'on veut ne pas montrer mais qui est déjà paramétrer automatiquement en "add to GIT",
on doit forcer manuellement. 
Pour cela on ouvre la console en bas puis on saisit "GIT rm + le nom du fichier" avec la mention "-f"(-force).
Exemple pour un fichier brouillon:
```
GIT rm brouillon -f
```
Le mot brouillon ou idéa en vert signifie qu'il ne sera pas pris en compte par GIT.

![brouillonenvert](/uploads/e13f30c23923767c6c8bfbbeb0aee952/brouillonenvert.PNG)
 