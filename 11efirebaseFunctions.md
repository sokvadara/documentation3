# FIREBASE FUNCTIONS
Typescript transpile en javascript et c'est un transcept de javascript car il donne des pouvoirs à javascript.  
Une des façon de travailler en typescript est de le paramétrer à la main afin qu'il fonctionne tout seul.  
L'autre façon(qui sera la notre) sera d'utiliser les functions de firebase qui permettent de faire du back-office avec les serveurs-less.  
Comme c'est en typescript, ça met déjà en place tout le système.  
**A savoir**: il y a plusieurs façons de faire du typescript.  

## Comment faire fonctionner typescript avec les firebase functions?
La première chose sera de créer un nouveau dossier pour travailler dessus.
On se place dans notre projet puis new directory que l'on nomme ici:  

image1

Puis on va dans le terminal et on vérifie d'être dans le bon dossier(pour nous on vérifie que try hard soit bien dans la racine).  
Puis on saisit successivement: cd tsSrc( cd signifie change directory) et firebase init functions:  

![image2](/uploads/6e77dd284a06d99e81b8fe2ff795045d/image2.PNG)
![image3](/uploads/951f8d6a0fccb658255d4ffd3373dcea/image3.PNG)

Ensuite,  
`are you ready to proceed? Y`  
On sélectionne `Use an existing project` puis son projet(pour nous new project TS).  
Puis `Typescript` et `TSLint? Y`(qui nous soulignera nos éventuelles erreurs).  
`Do you want to install dependencies with npm now? Yes`  
Une fois installé, on synchronise(certains pc synchronisent tout seul):   

![image4](/uploads/7512a7dfa468cc4ec61230f33b00eb5e/image4.PNG)

Si des fichiers apparaissent en rouge(pour les rendre vert):  

![image5](/uploads/76b40ec21185134f9f7d206bae04ef69/image5.PNG)

L'architecture du projet est alors formée:  

![image6](/uploads/997ee7ea4ab89a8d61c5aad7046538f9/image6.PNG)

et index.ts est le fichier dans lequel on va coder en typescript.  

Nous pourrons apercevoir plusieurs gitignore ou package json.  
Dans le package.json, il y a des scripts qui sont pré-fabriqués et qui permettent de lancer des commandes automatiquement.  

![image7](/uploads/59b73ceb43b39e085b972d800c108517/image7.PNG)

Une nouvelle fenêtre apparaitra alors, on va tester si cela fonctionne.   
On se met sur notre page :
```
Sur la page index.ts
console.log('coucou');
```
![image8](/uploads/1623d4d945e62724688731f539748b66/image8.PNG)

On double-clique sur serve.  
On voit dans le terminal coucou et:  

![image9](/uploads/57cc634331757f93917244d7dd90fc8b/image9.PNG)

En node, on faisait **require** quelque chose.  
En Typescript, on va utiliser le mot clé **import**:  

![image10](/uploads/7ed08b60e10cbe9b3d4a97106ec1a323/image10.PNG)

Ensuite comme dans l'exemple:  

![image11](/uploads/35c99e109d9b71fdc014ee578c9028f9/image11.PNG)

On reclique sur serve ou la petite flèche verte pour relancer.  
On voit alors une nouvelle Url:  

![image12](/uploads/3ec2c752d108892ab3ead81f2ff88e9d/image12.PNG)  

Si on change "hello word" par "test", test s'affichera à la place d'hello world dans le terminal.   
C'est comme cela que l'on nomme les fonctions.   
Si on clique sur le lien, on aura bien la réponse "Hello from Firebase".   

## METHODE POUR NE PAS RELANCER LE SERVEUR
Il faut faire une petite modification dans le panier de configuration du package.json.   
On va rajouter une nouvelle commande que l'on va appeler **watch**:  
![image13](/uploads/f6777172f8a728ad14dc2f16b8139efa/image13.PNG)

**tsc** signifie compile en typescript.  
**-w** signifie de regarder tous les changements sur le code et recompile.  

![image14](/uploads/e4500e15d86200fe314c426d42765a08/image14.PNG)

Si on modifie en mettant firebase42, on obtiendra le changement dans l'index.js et dans la page html.  
![image15](/uploads/369b6f87d638041c2ba9c844987941db/image15.PNG)
![image16](/uploads/97e21d257f6e562bade79063022b38de/image16.PNG)

par conséquent serve et watch tourneront en parallèle:  
![image17](/uploads/5f155e3c5442a3a4a45fc4937c811ba6/image17.PNG)

## POUR FAIRE DU EXPRESS  
On va dans le terminal et:  

![image18](/uploads/0b614be5424300bcde36c9067946dc43/image18.PNG)

Puis on récupère le lien dans la doc express:

![image19](/uploads/ff4a3ad5342940a16ac29af77e139e11/image19.PNG)

que l'on copie-colle dans notre **index.ts** et on le modifie:  

![image20](/uploads/679929337094b9da9ff86d4933e3b108/image20.PNG)

On peut alors ramener nos **app** de notre index.js  
Dans typescript, on va déclarer les types que l'on va utiliser pour nos data.  
Dans un 1er temps, on peut créer dans **src** un nouveau dossier que l'on va appeler models.   
Du coup, on va commencer par modéliser de façon intellectuel les objets que l'on utilisera dans l'application.  
On crée alors 2 fichiers dans models: hotel.models.ts et room.models.ts  

![image21](/uploads/03aae7892792263cc194da906a025bf5/image21.PNG)

En général, quand on fait un model, on commmence par l'élément le plus profond, ici les room.    
On peut pour nous aider copier le model d'une room que l'on mettra en commentaire.  
Ensuite, **export** qui est un mot clé qui signifie que quand on va déclarer soit une fonction soit une constante, on pourra y accéder depuis un autre fichier.  
C'est ici que l'on pourra importer et exporter des choses qui viennent de fichiers différents.  
**interface**: définition de ce qu'il y a dans notre model.   

![image22](/uploads/95ed1dccc89efd49cbe664f58eb22471/image22.PNG)
Ici, on va mettre le type de chaque clé qui compose notre room.  
On effacera alors le modèle en commentaire.  
On fera la même chose avec hotel.  

![image23](/uploads/b40b79da1f09c5165f9fc57c290fa430/image23.PNG)
On peut observer que room.model a été importé automatiquement du fichier d'à coté.   
Si cela ne se fait pas, RoomModel apparaitra en rouge, il faudra alors se positionner dessus et faire `alt entrée`.  
Et comme on veut l'array de Roommodel, on oubliera pas de rajouter le tableau vide:   

![image24](/uploads/96fcb75e979e78e1b90e929c1cdfe7ab/image24.PNG)

On va ensuite créer un nouveau fichier que l'on va mettre à la racine:  

![image25](/uploads/e64b66f0bfab59aed3015f559f5f923f/image25.PNG)  

et on va y copier nos hotels.  
On mettra **export const hostels** et on précisera le type de hostels(HotelModel).   

![image26](/uploads/c2060afd3d6956e39077055f8bd6511f/image26.PNG)

Il ne faudra pas oublier de rajouter le tableau vide **[]** sous peine d'un message d'erreur.   
On observe que la fonction s'est importée automatiquement.  

L'étape suivante consiste à créer un nouveau fichier dans **src** pour y mettre nos fonctions de l'**index.js**,    
on le nommera **utilities.ts**.  
Dans un 1er temps, mettre **export** devant chaque function, on pourra alors y avoir accès à l'extérieur.   
On précisera aussi le type pour éviter les messages d'erreur, tout doit être typé.  
Ainsi "hostel, buffer" etc seront typé: 

![image27](/uploads/a6913195197b757b9ddfb29ab8e9bc88/image27.PNG)
 
On voit aussi le message d'erreur pour **"hostels"** de **buffer.id**.  
On se place dessus et `alt entrée`, il nous l'importe directement de la **database**.  

  











