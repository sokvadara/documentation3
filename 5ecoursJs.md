## LE CONCEPT DES ARRAYS
# QU'EST CE QU'UN ARRAY? COMMENT LE MANIPULER ET A QUOI SERT-IL?
On peut faire du javascript dans n'importe quelle page HTML.
Tout d'abord on peut créer une page "index.js" et vérifer si elle fonctionne avec:
```
console.log('coucou');
```

**QUELLES SONT LES NORMES?** la norme représente la façon dont on va coder pour que tout le monde puisse comprendre.
Ainsi, il est important de la respecter pour pouvoir élaborer de grands projets.
En JS, la norme sera d'utiliser les simples cotes, de finir chaque instruction par un "point virgule".
Ne pas oublier de lier le fichier JS et de vérifier dans la console.
Quand on met le signe "=", ne pas oublier de mettre un espace avant et après.
Pour nous faciliter le bon code, on fait:
```
alt ctrl L 
Les normes se placent automatiquement
```

**Ce qui est important dans un ARRAY ( tableau)**
un ARRAY peut-être tout un objet, un container dans lequel on peut mettre d'autres choses.
Comment l'écrire: 
```
Pour l'instant le tableau est vide
const tab = []
On commence à remplir: 
const tab = [ 1, 2, 3, 4, 5, ];
```
REMARQUE: mettre la norme avant de commiter. Prendre l'habitude de "ctrl alt L" régulièrement.
Dans un ARRAY, on peut donc mettre des nombres, des strings, des booléans, des prédicats.

Rappel des prédicats et booléans.

A savoir, on peut mettre dans un tableau une suite d'objets de genre différent mais pour l'utiliser il est préférable d'en mettre du même genre.

## LES FONCTIONS

Si on veut savoir combien il y a d'éléments dans son tableau, on fait : 
```
console.log (tab.length);
```

Si on veut récuperer un élément dans un tableau, on utilise l'index:
```
const tab = [ 1, 4, 3, 2]
On suppose que l'on veut le chiffre 3. Alors:
console.log (tab[2]);
On compte à partir de 0.

Une bonne façon d'obtenir le dernier élément d'un tableau
console.log(tab[tab.lenght -1]);
```

**IMPORTANT**: toujours faire attention à la qualité(esthétique) du code.
 
Quand on appelle un indice qui n'existe pas, on aura le résultat "NAN".

## LES OPERATEURS
Un opérateur se met toujours à la fin d'un tableau.
```
tab.forEach(element = > console.log(element))
La boucle forEach va prendre le 1er,2e,3e etc élément du tableau et va lui appliquer sa fonction.
```
Un autre exemple, on ne veut pas afficher l'élément mais juste l'âge:
```
tab.forEach(element => console.log(element.age))
on n'aura alors les âges dans la console.
Pour vieillir tout le monde de 10ans
tab.forEach(element => console.log( element.age+10))
Tout le monde sera vieillit de 10 ans dans la cosnole.
Pour doubler l'âge de tout le monde: 
tab.forEach(element = > console.log(2*(element.age))
function {return*2}
```
```
tab.map(element => element.age)
Je crée ma nouvelle constante:
const tableauDesPrénoms= tab.map(elemnt=> element.firstName)
console.log(tableauDesPrénoms)
On se retrouve alors avec les prénoms dans la console.
```
La différence entre tab.forEach est que tab.map va juste sélectionner une partie de l'élément.

On peut aussi avoir des transformations plus complexes.

```
const tableauDesAges*10 = tab.map(element = > element.age*10)
console.log(talbeauDesAges*10)

const moinsDe40 = tab.filter(element = > element.age<40)
console.log(moinsDe40);

const ordererTab = tab.sort(compareFn:(a,b)=> a.age - b.age)
ou 
const ordererTab = tab.sort(compareFn:(a,b)=> b.age - a.age)

const areAllMajors = tab.every(element => element.age > 18);
console.log(areAllMajor);
```










 