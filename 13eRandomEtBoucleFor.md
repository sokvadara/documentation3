 # RANDOM 
Il faut savoir qu'en informatique, l'aléatoire n'existe pas.  
En effet, lorsqu'on fait un Random, on veut un chiffre aléatoire sauf que ce chiffre ne peut pas l'être car il est basé sur l'horloge de notre pc.  
Ce chiffre dépend de l'heure à laquelle on fait la commande.  
On peut alors parlé d'un chiffre "pseudo aléatoire".  

Pour lancer la commande `random` il faut appeler la librairie `Math`.  
**Bon à savoir**: le JS n'est pas du tout recommandé pour faire des opérations mathématiques.
  
![image1](/uploads/91b07e00beede0534e0e387034b64bd6/image1.PNG)
  
Résultat dans la console:  

![image2](/uploads/38caf2324d2a3f2f10f42ee9841b6f94/image2.PNG)


et pour avoir le bon résultat, il faut faire:  

![image3](/uploads/5bf5c0cf46a918d1f6db114a69a45060/image3.PNG)

Le résultat est bien évidemment 0.3  

Pour appeler la commande `random`:  

![image4](/uploads/c07cc0227133bdb724b0c80a146e6fca/image4.PNG)

On obtient alors un chiffre entre 0 et 1.  

Pour avoir un chiffre entre 1 et 10: 
 
![image5](/uploads/c5761e4b8f643ee28c1cb77ed985a99e/image5.PNG)

Exemple d'un résultat de l'aléatoire:  

![image6](/uploads/4f26f3a54534f96895224d205b777adc/image6.PNG)

et pour enlever les chiffres après la virgule, on utilise `flor`:  

![image7](/uploads/059280bf9f5117fbe0c88a3465d8af3f/image7.PNG)


Cela devient plus intéressant pour créer une base de données d'utilisateurs aléatoire. 

![image8](/uploads/ceea609ca8d65b46e456a7214f62a1ac/image8.PNG)

Les prénoms vont alors apparaître de manière aléatoire. 

 ## BOUCLE FOR  
C'est un outil qui permet de faire des boucles comme avec un map sauf qu'un map ou un forEach boucle sur les éléments d'un tableau alors qu'avec le for, on décide sur quoi on boucle.  
Il y a 2 parties: 

![for2parties](/uploads/689a049d6cd02300ee1d0f084c24d846/for2parties.PNG)

La partie conditionnelle `()`dans laquelle on exprime les conditions de la boucle.
La partie où l'on va exécuter ce qu'il y a dans la boucle `for` équivalente à la fonction de `call-back` dans un `forEach ou un map`.  

La 1ère partie étant elle-même composée en 3 partie(qui sont optionnelles):  
![1erePartieFor](/uploads/31891faa857257cc61b5a0334caeef3a/1erePartieFor.PNG)

La 1ère est celle de l'initialisation`(init)`, elle peut n'avoir aucun rapport avec la boucle **for**.  
C'est une expression de code evalué au démarrage de la boucle **for**.  
On peut y mettre ce que l'on veut(un console.log ou même la laisser vide):
```
for( ; i< names.lenght; i++){
};
```
La 2ème c'est celle qui sert à savoir la condition dans laquelle va continuer à jouer la boucle **for**.  
Cela ne sera pas du code exécuter mais ça va renvoyer un prédicat.  
On peut aussi y mettre ce que l'on souhaite sans qu'il y ait de rapport avec la boucle for:  
![exemple1](/uploads/98abd13756c42230d9724d7139a67d03/exemple1.PNG)Ici, la boucle se jouera toujours.
![exemple2](/uploads/851abeb1757369260386da2f294b8504/exemple2.PNG)Ici, la boucle ne se jouera jamais.
![exemple3](/uploads/06bf934e0db440d9555d6d5bd9df7a64/exemple3.PNG)Ici, la boucle ne se jouera pas car names.lenght fait 3.  

Si on veut que la fonction s'exécute une fois et s'arrête:  
![exemple4](/uploads/82747fd11e241126dcb440a18ab8d552/exemple4.PNG)(tableau vide)  

La 3ème c'est du code exécuter lorsque la boucle est finie, ce n'est pas toujours `i++`.  
On peut aussi très bien la laisser vide.  

## QUELQUES EXEMPLES DE BOUCLE FOR
![exemple9](/uploads/128e63bf07a48bec5dde03e5c0243235/exemple9.PNG)
Ici 
```
let i = 0 // let est égal à 0
i est alors inférieur à names.lenght dont la valeur est 3.
console.log(names[i]) // va alors afficher dupont
Enfin cela continue avec le i++ jusqu'à ce que i ne soit plus inférieur à 3
```
On met **lenght** pour faciliter tout changement dans le tableau.  
Pour avoir un nom sur 2, on met `i+=2`.  
Pour commencer par le 2ème nom, `let i = 1`.  

Un autre exemple:  
![exemple10](/uploads/9233afa7fab5401130baf3f5c16fa2c3/exemple10.PNG)  
Ici, on obtient un tableau de 10 lastname et firstname de façon aléatoire.
Un âge de 0 à 99 ans et un code postal de 10000 à 90000.

## QUELQUES ABREVIATIONS A SAVOIR
```
i++ // signifie i = i + 1
i-- // signifie i = i - 1 
i+=2 // signifie i = i + 2
i-=2 // signifie i = i - 2
i*=2 // signifie i = i * 2
i/=2 // signifie i = i / 2
```
## LE MODULO
```
console.log(10%2); // console.log 10 modulo 2
0 // dans la console:  
console.log(10%3); // console.log 10 modulo 3
1 // reste 1
```
S'il y a un reste, cela signifie que 10 n'est divisible par 3.
S'il n'y a pas de reste, le nombre est donc divisible.

## Faire planter un pc et solution
![exemple5](/uploads/1b13ac7e9aa1c656a4889f45ec041320/exemple5.PNG)  
Sélectionner le bon UC dans le gestionnaire de tâches.
![exemple6](/uploads/0987dd41eed346b4fa71ba14b11358f4/exemple6.PNG)



