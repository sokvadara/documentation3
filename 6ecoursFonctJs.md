# LES FONCTIONS EN JS

![fonctionDeclaration](/uploads/8ac57bffdd4318e0c0c74596335e8e1d/fonctionDeclaration.PNG)

```
return void
```
void signifie rien, la fonction ne renvoie rien.

## LA FONCTION REVERT

Pour annuler des modifications, on va utiliser Revert

![boutonRevert](/uploads/e2f4f4fba87aea7aef34b0b2d421c23e/boutonRevert.PNG)

![validerRevert](/uploads/b34f1facee3f9871f92f63615978ad03/validerRevert.PNG)

Les fonctions, les paramètres 

Les paramètres sont obligatoires sinon cela signifie que l'on fait appelle à toute la fonction alors que l'on veut le résultat.

Pour qu'une fonction soit interractive, il faut lui rajouter des paramètres?  
Ici x et y:

![x__y](/uploads/82be5e74f2ee42b399d4071d6d516aed/x__y.PNG)


Ne pas oublier de mettre une valeur de retour lorsqu'on pense en avoir besoin, sinon ne pas la mettre.
Attention: lorsqu'on tombe sur un return, la fonction arrête de s'exécuter quelque soit le code qui suit.

![newreturn](/uploads/4c659e6b0c9e7947cd72f7ded4a7d71a/newreturn.PNG)
La console renverra 0.  
Il faut penser à la valeur de retour pour éviter d'avoir un résultat undefined, par exemple:  

![return](/uploads/985b5e5c72a336308e4c38b0720f118f/return.PNG)
On aurait pu mettre return 0, etc...

Il faut faire attention à ce que la fonction retourne à une valeur du même type et garder une cohérence.
Un string renvoyant de un string et un number renvoyant un number.

Il est préconiser de limiter la fonction à 4 arguments.

![function](/uploads/047ff2a0f0dac2335ca3da596aa6d348/function.PNG)

Lorsqu'on ne sait pas combien de paramètres on va avoir, on utilise ...arg ou ...Z
Les trois petits points( **rest operator**) signifient de prendre tous les arguments et de les mettre dans un array. 
Une fois dans l'array, on peut le traiter comme on le souhaite.

![restoperator](/uploads/50da3de600c67b9b5081e0c0a3163538/restoperator.PNG)

![capreduce](/uploads/a8918756db8e32ae188237f681a5b4e5/capreduce.PNG)

Le **rest operator** doit être écrit en dernier dans les arguments et on ne peut en avoir qu'un seul.

Par exemple, dans l'exo1 on peut changer le résultat en fonction:

![capexo1](/uploads/730e002a3ec2e91dcb6126a7d05445f4/capexo1.PNG)

qui devient: 

![new](/uploads/cb00dbf9311c491daa1d72f6a14cc6c9/new.PNG)

![nouvelle](/uploads/bf151dec9bab98b15aad161407a807b3/nouvelle.PNG)

Si on veut les combiner:

![combfonct](/uploads/705ce7f1f17737ba78d191833eedd2bd/combfonct.PNG)

## LE SPREAD OPERATOR

Il sert à copier un objet, du coup créer un nouvel objet et de les fusionner à la fois.