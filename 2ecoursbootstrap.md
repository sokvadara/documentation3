# 2ème cours: Bootstrap  
Un navigateur est un parseur(parser = parcourir) qui en fonction de ce qu'il va lire, déclenchera des actions.  

## Quelle est l'architecture lorqu'on fait un site internet statique?  
1ere question: combien de pages? car on va créer autant de dossiers que de pages.
Par exemple pour un cv, on peut faire 3 pages, un "home page" encore appelé "landing page", un "about" et un "faq".
Du coup 3 directory.  
Ensuite une page pour contenir les assets que les 3 pages vont pouvoir venir chercher.
Dans asset, on peut déja commencer à mettre un dossier img et un dossier vendors( tous les scripts, html ou css qui ne viennent pas de nous).
On peut aussi commencer à mettre des images dans assets et les ajouter dans GIT(si pas proposer clique droit puis ajout dans GIT).
On peut aussi mettre dans asset "Font"(police de caractère).
D'abord la 1ere page, on créer "new HTML file" que l'on appelle "index".

## 3 balises à connaitre pour le référencement: 

``` html 
<section>
    <header></header>
    <footer></footer>
</section>
```
Du coup, pour 3 parties:
``` html 
<section>
    <header></header>
    <footer></footer>
</section>

<section>
    <header></header>
    <footer></footer>
</section>

<section>
    <header></header>
    <footer></footer>
</section>
```
Google comprendra alors qu'il s'agit de 3 parties centrales.  
## Important: un h1 par page.  
Ne mettez pas de footer si vous n'avez rien à mettre dedans et titrez(h1, h2...h6).  
Commencez toujours par le coeur des informations avec la structure, le css venant après.  

## Utilisation de Bootstrap   
Copier les links dont vous avez besoin.  
Par exemple, "card title", "button", "badge" "breadcrumb" etc... 
 
![Capturepourbadge](/uploads/3ecaabadb67a3bdeb6613fc509c0e88d/Capturepourbadge.PNG)

Le bouton bleu est appelé call to action(CTO).
   
![Capturecalltoaction](/uploads/ab793c12051512b87fc48ad383f7e83b/Capturecalltoaction.PNG)

## 2 balises à connaître:  
div en diplay block( définir des blocks)  
span en display inline( des mots collés les uns aux autres)  

Attention à l'emplacement de vos fichiers(par exemple les IMG) 

![Captureemplacement](/uploads/ce2795341ad74143cafb426899a61889/Captureemplacement.PNG)
 
## CONTAINER, ROW ET COL
Un site qui utilise bootstrap doit être dans un container(sorte de div principal dans laquelle bootstrap vous oblige à mettre son contenu).  
Par conséquent il faudra créer une classe container. 
Le container permet une meilleure responsivité du site et container-fluid signifie que le contenu prendra la plus grande place possible.
   
![Capturecontainerfluide](/uploads/fec5ac99c2716333f235b2ca262cf8a4/Capturecontainerfluide.PNG)

Il faut organiser le site en fonction de lignes et de colonnes. Pour ce faire, on utilise les "row et col" en créant des "class".  
Un row est toujours accompagné d'un col.Sachant que la page est divisée en 12 col. Si l'on met simplement "col", cela contiendra les 12 col. 
   
![Capturerowetcol](/uploads/18fcb552815e6ccd1aa4dc98c74d54b8/Capturerowetcol.PNG)








