# Sokvadara Chouk
## 178 avenue Henri Barbusse
## 93700 Drancy

## **EXPERIENCE**  
### **CHAUFFEUR DE TAXI PARISIEN | Indépendant | Depuis Janvier 2009**  
1. Prise en charge de la clientèle  
2. Fidélisation de la clientèle  

## **AIDE CUISINIER | Pasta Pit'zz | Juin 2007 - Mai 2008**  
1. Préparation des pizzas  
2. Accueil et conseil à la clientèle  

## **OPERATEUR DE SURETE QUALIFIE | Alyzia sûreté | Avril 2003 - Décembre 2008**  
1. Contrôle des cartes d'embarquement et des bagages cabines  
2. Accueil et guide des passagers  

## **DIPLOME**  
### DUT Gestion des Entreprises et des Administrations  
### Université Paris 13 | 2002 - 2004

## **COMPETENCE CLES**  
1. Rigueur et organisation  
2. Autonome  
3. Sens du relationnel   

## **CENTRE D'INTERET**  
1. Sport(course à pieds, football)  
2. Voyages  


