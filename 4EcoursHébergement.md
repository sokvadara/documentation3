# Cours sur l'hébergement/CDN.

CDN: Content délivery network, un réseau de mise à disposition de contenu.
Un simple fichier dans un site internet est un contenu, à partir du moment où il est  mis à disposition, il représente un contenu.
Un fichier texte, une vidéo, une image est un contenu. Ces contenus sont appelés des ressources.
Par conséquent, un CDN est un ensemble de site internet situé à travers le monde, qui copie les mêmes ressources afin d'être disponible partout.

## COMMENT DEPLOYER NOS RESSSOURCES?

On peut utiliser l'outil de google qui se nomme Firebase.
Dans un 1er temps, on se crée un compte. 
Ensuite on aperçoit la console qui représente l'espace de travail principal sur lequel nous pouvons mettre nos projets.
On commence par "ajouter un projet"

![imageprojetrécent](/uploads/c1ebe3c745edbf398f435713697fff24/imageprojetrécent.PNG)

Ensuite, on peut commencer avec test promo 2 et désactiver Google Analytics puis créer un projet.

## A QUOI SERT FIREBASE?
 
Permet de gérer l'authentification, des bases de données, le stockage d'images et dans notre cas le hosting(notre cdn).
Grâce au cdn, on peut diffuser en quelques secondes des données dans le monde entier.

Dans un premier, se mettre sur le webstorm et sur le fichier que l'on veut diffuser.
Pour mémoire, nous devions avoir 2 ripos GITLAB.

![installationde_firebase](/uploads/471f7f8a636301e0ca4c006b861aece1/installationde_firebase.PNG)

Puis on copie/colle dans la console.
A ce moment il va installer les outils qui vont permettre à firebase de se déployer à l'extérieur.

Ensuite, il va y avoir 2 commandes à mettre, le login et le init:

![2commandesenplus](/uploads/20cb877b9a28111bddea63cfa109fdd6/2commandesenplus.PNG)

Login, il faut se loguer avant et init pour initialiser.
On saisit alors dans la console firebase login, et firebase init hosting pour éviter qu'il réinitialise toute la console.
**Attention d'être dans le bon dossier.**
On valide alors pour commencer. On choisit le projet " use an existing project"(ici mon cv).

### QUELLE EST LE PUBLIC DIRECTORY?
C'est le dossier qui va nous servir à envoyer toutes les informations sur le net, celui qui va être publique pour les utilisateurs.
Notre structure étant faite pour GITLAB, elle n'est pas optimisée pour le déploiement.
On va alors créer un dossier que l'on nomme "public" puis on colle les dossiers à l'intérieur.

![copiercollerdanspublic](/uploads/afd63c3b4822ecc9f459f40234a881e6/copiercollerdanspublic.PNG)

Une autre modification est d'ouvrir la page "Home" et de copier coller son contenu dans public( car c'est la racine du projet).
Si dans la page Home, il y avait du JS ou du CSS, on copie colle aussi. La page home ne servant plus, on peut la delete.

![contenuhome](/uploads/3f55229a89f7e3e2fdf82d11e29878a0/contenuhome.PNG)

L'avantage avec webstorm, c'est que lors d'un copier coller, les liens se changent automatiquement.
L'architecture est alors prête pour le CDN.

Dans la console:
il demande si le directory est le public, on valide.
sommes-nous sur un single page application( single page en angular ou react? no
voulez-vous écrasez le fichier index.html? NO
Une fois l'initialisation complète, on fait "firebase deploy".Il prendra alors tout ce qu'il y a dans public pour l'envoyer sur internet.
Il crée alors une adresse URL pour hosting et lorsque l'on clique dessus, notre site apparait sur internet(en "SSL"= connection sécurisée).

On voit alors apparaitre quelques fichiers en rouge que l'on va corriger.

![fichiersrouges](/uploads/4eaa1e8c8f4f7f1ba8d25b0a085da50d/fichiersrouges.PNG)

Du coup, on met ".firebase" dans le gitignore.
On gardera ".firebaserc"(info sur notre projet) et "firebase.json"(indication de notre ripo).
On fera juste un GIT add et un push vers GIT.
On a alors un site statique.