 # MA DOC PERSO
 ## **[]forEACH()**
![forEachArrayMdn](/uploads/78006f579f080aca9b44c95f39c49f03/forEachArrayMdn.PNG)

 ## **[]sort()**
![sortArrayMdn](/uploads/bed445cb3352f64f647d190f5f9dbde4/sortArrayMdn.PNG)

 ## **[]map()**
![mapArrayMdn](/uploads/c9c5e7dc58d9ab5d1539fe51b1be6ea2/mapArrayMdn.PNG)

 ## **[]filter()**
![filterArrayMdn](/uploads/8859d8659ecaa5bd2348c5376ff0e3a0/filterArrayMdn.PNG)

 ## **[]lenght()**
![lenghtArrayMdn](/uploads/06c3a8475e2138e58a74e22163d77707/lenghtArrayMdn.PNG)

 ## **[]concat()**
![concatArrayMdn](/uploads/795843eb95640d30a706f0bda2d1d64a/concatArrayMdn.PNG)

 ## **[]reduce()**
![reduceArray.MdnPNG](/uploads/91217c9b03ed3ea1609a3ac416f116fd/reduceArray.MdnPNG.PNG)

 ## **[]find()**
![findArrayMdn](/uploads/2e9cfbbfd4dd56709f5f0cfc24c4841e/findArrayMdn.PNG)

 ## **[]findIndex()**
![findIndexArrayMdn](/uploads/a61e031c40ccf87c01a49137c45c954b/findIndexArrayMdn.PNG)

 ## **[]index()**
![indexOfArrayMdn](/uploads/f5ea267dd96e675ac086d43691816b4d/indexOfArrayMdn.PNG)

 ## **[]push()**
![pushArrayMdn](/uploads/005f46ca42585b0d2a8c4206ec481c04/pushArrayMdn.PNG)

 ## **[]join()**
![joinArrayMdn](/uploads/fe4eae947d386588374f1523786941fa/joinArrayMdn.PNG)

 ## **[]some()**
![someArrayMdn](/uploads/29de884009df3d3a95c1fda0792330aa/someArrayMdn.PNG)

 ## **[]every()**
![everyArrayMdnPNG](/uploads/d3da1605c277ea9862bf70678b193849/everyArrayMdnPNG.PNG)

 ## **[]splice()**
![spliceArrayMdnPNG](/uploads/429a97486ad62ed62b67cbd6c9acd701/spliceArrayMdnPNG.PNG)

 ## **split**
![splitMdn](/uploads/8f6bc67913a533b39c29519bc8fc1ea3/splitMdn.PNG)